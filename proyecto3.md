# Proyecto #3 HackAPI

Una imagen vale más que mil palabras, ¿no? Actualmente el internet se expresa mediante imágenes, una tendencia de expresión mediante imágenes en internet es mediante [memes](https://en.wikipedia.org/wiki/Internet_meme), estos memes no son más allá que fotos que tienen texto y que generan un contexto. Ej: [forever_alone](http://weknowmemes.com/wp-content/uploads/2012/04/forever-alone-hipster.jpg)

Para crear un meme existen diferentes sitios web para tal fin, una página muy cool es [meme_generator](http://memegenerator.net/) donde pueden elegir una imagen meme y hacerla customizada.

MemeGenerator tiene un [API](http://version1.api.memegenerator.net/) :D, por lo cual el grupo docente de Hack le pide a sus alumnos que usen el API de MemeGenerator para ofrecer un servicio que:

* Maneje registro de usuarios.
* Tenga Autenticación hecha por ustedes.
* Puedan verse los memes más comunes para ser cool.
* Los usuarios puedan crear los memes via MemeGenerator y ser guardados en el sistema.
* Los memes puedan ser categorizados.
* Los memes puedan ser tageados.
* Los memes puedan ser enviado por correo electrónico.
* Los memes puedan ser enviados a una red social de su preferencia (Facebook está prohibido por su ranciedad).
* Los usuarios puedan elegir si su meme es privado o no, en caso de que un meme sea público, este puede ser listado por cualquier usuario.
* Los memes públicos pueden ser rankeados.
* Los usuarios no registrados solo pueden ver un listado de los memes más populares.


Para ello debe elegir Ruby on Rails sobre MySql o PostgreSQL para llevar a cabo sus servicios. Queda explícito que el sistema debe manejar parámetros para que los datos a devolver no sean tan extensos, es decir, paginación.

Cualquier gema a usar deberá ser consultada por el canal #clases-y-retos mencionando al Mentor Daniel Espinoza o la preparadora Johanna Salazar.

Para la autenticación de usuario no podrá usar devise o cualquier gema asociada. Se recomienda usar los headers X- para saber si una petición corresponde a un usuario registrado.

El registro de usuario debe hacerse via API JSON, así como la autenticación.

La entrega del proyecto es de la siguiente forma:

* Bajo plataforma gitlab.
* Primera ronda de entrega: Domingo 31 de enero antes de las 23:59 GMT -4:30.
* Segunda ronda de entrega: Viernes 5 de febrero antes de las 23:59 GMT -4:30.
* Última ronda de entrega: Domingo 7 de febrero antes de las 23:50 GMT -4:30.

Luego de la entrega formal no puede hacerse otra entrega.

El proyecto consta de parejas de dos en duetos.


* Enmanuel - Frank.
* Letty - Gabriel.
* Ultrafunk - Jose Rangel.
* Nacho - Luis.
* Oscar - Jorge.
* Alex - Carlos.
* Julius - Roman.
* Armando - Jose Leon.
* Jose Rodriguez - Erick.

Bonus points (Calcomania):

* Tiempo de Entrega.
* Autenticación de usuarios.
* Arquitectura y documentación.

Hyper Bonus Point (Taza):
* Entrega el domingo 31 + [Heroku](http://heroku.com).
